FROM tomcat
MAINTAINER Ellihtap

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/smp.war
EXPOSE 8080
CMD ["catalina.sh", "run"]
